/***** DELETE ALL DATA *****/
delete from profile_settings;
delete from profile_apps;
delete from profile;
/***** INSERT DEMO DATA *****/

/* 1. INSERT INTO PROFILE */
insert into profile (account_id, vin, status)
values ((select account_id from account_entity where email = 'zac.grantham@gm.com'), '2G1105S34H9128065', 'Active');
insert into profile (account_id, vin, status)
values ((select account_id from account_entity where email = 'aaron.johnson@gm.com'), '2G1105S34H9128065', 'Active');

/* 2. INSERT INTO PROFILE_APPS*/
/* Profile: Zac Grantham */
insert into profile_apps (ext_app_id, app_version_id, name, profile_id)
values ((select app_id from catalog_app_version where short_description='Starbucks'),
        (select version_id from catalog_app_version where short_description='Starbucks'),
        'Starbucks',
        (select id from profile where
                account_id = (select account_id from account_entity where email = 'zac.grantham@gm.com') and vin = '2G1105S34H9128065'));

insert into profile_apps (ext_app_id, app_version_id, name, profile_id)
values ((select app_id from catalog_app_version where short_description='Weather Channel'),
        (select version_id from catalog_app_version where short_description='Weather Channel'),
        'Weather Channel',
        (select id from profile where
                account_id = (select account_id from account_entity where email = 'zac.grantham@gm.com') and vin = '2G1105S34H9128065'));

insert into profile_apps (ext_app_id, app_version_id, name, profile_id)
values ((select app_id from catalog_app_version where short_description='Glympse'),
        (select version_id from catalog_app_version where short_description='Glympse'),
        'Glympse',
        (select id from profile where
                account_id = (select account_id from account_entity where email = 'zac.grantham@gm.com') and vin = '2G1105S34H9128065'));

insert into profile_apps (ext_app_id, app_version_id, name, profile_id)
values ((select app_id from catalog_app_version where short_description='Audiobooks.com'),
        (select version_id from catalog_app_version where short_description='Audiobooks.com'),
        'Audiobooks.com',
        (select id from profile where
                account_id = (select account_id from account_entity where email = 'zac.grantham@gm.com') and vin = '2G1105S34H9128065'));


/* Profile: Aaron Johnson */
insert into profile_apps (ext_app_id, app_version_id, name, profile_id)
values ((select app_id from catalog_app_version where short_description='Starbucks'),
        (select version_id from catalog_app_version where short_description='Starbucks'),
        'Starbucks',
        (select id from profile where
                account_id = (select account_id from account_entity where email = 'aaron.johnson@gm.com') and vin = '2G1105S34H9128065'));

insert into profile_apps (ext_app_id, app_version_id, name, profile_id)
values ((select app_id from catalog_app_version where short_description='Weather Channel'),
        (select version_id from catalog_app_version where short_description='Weather Channel'),
        'Weather Channel',
        (select id from profile where
                account_id = (select account_id from account_entity where email = 'aaron.johnson@gm.com') and vin = '2G1105S34H9128065'));

insert into profile_apps (ext_app_id, app_version_id, name, profile_id)
values ((select app_id from catalog_app_version where short_description='Glympse'),
        (select version_id from catalog_app_version where short_description='Glympse'),
        'Glympse',
        (select id from profile where
                account_id = (select account_id from account_entity where email = 'aaron.johnson@gm.com') and vin = '2G1105S34H9128065'));

/* INSERT INTO PROFILE_SETTINGS*/
/* Profile: Zac Grantham */
insert into profile_settings (category, name, selected_value, timestamp, type, profile_id )
values ('Audio', 'XMRadio1', '11', CURRENT_TIMESTAMP, 'INT',
        (select id from profile where account_id = (select account_id from account_entity where email = 'zac.grantham@gm.com') and vin = '2G1105S34H9128065'));

insert into profile_settings (category, name, selected_value, timestamp, type, profile_id )
values ('Audio', 'XMRadio2', '23', CURRENT_TIMESTAMP, 'INT',
        (select id from profile where account_id = (select account_id from account_entity where email = 'zac.grantham@gm.com') and vin = '2G1105S34H9128065'));

/* Profile: Aaron Johnson */
insert into profile_settings (category, name, selected_value, timestamp, type, profile_id )
values ('Audio', 'XMRadio3', '18', CURRENT_TIMESTAMP, 'INT',
        (select id from profile where account_id = (select account_id from account_entity where email = 'aaron.johnson@gm.com') and vin = '2G1105S34H9128065'));

insert into profile_settings (category, name, selected_value, timestamp, type, profile_id )
values ('Audio', 'XMRadio1', '32', CURRENT_TIMESTAMP, 'INT',
        (select id from profile where account_id = (select account_id from account_entity where email = 'aaron.johnson@gm.com') and vin = '2G1105S34H9128065'));