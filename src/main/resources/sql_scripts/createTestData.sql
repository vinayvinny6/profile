/* INSERT INTO PROFILE */
insert into profile (account_id, vin, name, status)
values (100, '100VIN1', 'VEH1-PROFILE100', 'Active');
insert into profile (account_id, vin, name, status)
values (100, '100VIN2', 'VEH2-PROFILE100', 'Active');
insert into profile (account_id, vin, name, status)
values (101, '101VIN3', 'VEH3-PROFILE101', 'Active');
insert into profile (account_id, vin, name, status)
values (102, '102VIN4', 'VEH4-PROFILE102', 'Active');
/* INSERT INTO PROFILE_APPS*/
insert into profile_apps (ext_app_id, app_version_id, name, profile_id)
values (1, 1, 'Spotify', 1);
insert into profile_apps (ext_app_id, app_version_id, name, profile_id)
values (2, 8, 'Audible', 1);
insert into profile_apps (ext_app_id, app_version_id, name, profile_id)
values (3, 2, 'CUE', 3);
insert into profile_apps (ext_app_id, app_version_id, name, profile_id)
values (4, 4, 'Navigation', 4);
/* INSERT INTO PROFILE_SETTINGS*/
insert into profile_settings (category, name, profile_id, selected_value, timestamp, type)
values ('Audio', 'XMRadio1', 2, '11', CURRENT_TIMESTAMP, 'INT');
insert into profile_settings (category, name, profile_id, selected_value, timestamp, type)
values ('Audio', 'XMRadio2', 2, '26', CURRENT_TIMESTAMP, 'INT');
insert into profile_settings (category, name, profile_id, selected_value, timestamp, type)
values ('Audio', 'XMRadio3', 2, '28', CURRENT_TIMESTAMP, 'INT');
insert into profile_settings (category, name, profile_id, selected_value, timestamp, type)
values ('Audio', 'XMRadio4', 2, '32', CURRENT_TIMESTAMP, 'INT');
insert into profile_settings (category, name, profile_id, selected_value, timestamp, type)
values ('VehicleSetting', 'SeatHeight', 3, '11', CURRENT_TIMESTAMP, 'INT');
insert into profile_settings (category, name, profile_id, selected_value, timestamp, type)
values ('VehicleSetting', 'WheelHeight', 3, '22', CURRENT_TIMESTAMP, 'INT');