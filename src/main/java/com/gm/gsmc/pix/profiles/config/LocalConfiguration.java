package com.gm.gsmc.pix.profiles.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile ("default")
@EnableFeignClients(basePackages = "com.gm.gsmc.pix.profiles.client.local")
public class LocalConfiguration {

    private static final Logger log = LoggerFactory.getLogger(LocalConfiguration.class);

    /*@Bean
    public AccountClient getAccountClient() {
        log.info("Loading Local Config - Account Client");
        return new AccountClientFallback();
    }
    
    @Bean
    public AppAvailabilityClient getAppAvailabilityClient() {
        log.info("Loading Local Config - App Availability Client");
        return new AppAvailabilityClientFallback();
    }
    
    @Bean
    public AppCatalogClient getAppCatalogClient() {
        log.info("Loading Local Config - App Catalog Client");
        return new AppCatalogClientFallback();
    }
    
    @Bean
    public VehicleClient getVehiclesClient() {
        log.info("Loading Local Config - Vehicles Client");
        return new VehicleClientFallback();
    }*/
}
