package com.gm.gsmc.pix.profiles.translator;

import com.gm.gsmc.pix.general.dao.AbstractModelTranslator;
import com.gm.gsmc.pix.model.profile.ProfileDetails;
import com.gm.gsmc.pix.profiles.jpa.model.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfileTranslator extends AbstractModelTranslator<Profile, ProfileDetails> {

    private final AppTranslator appsTranslator;
    private final SettingsTranslator settingsTranslator;

    @Autowired
    public ProfileTranslator(AppTranslator appsTranslator, SettingsTranslator settingsTranslator) {
        this.appsTranslator = appsTranslator;
        this.settingsTranslator = settingsTranslator;
    }

    @Override
    public ProfileDetails translate(Profile original) {
        ProfileDetails profile = new ProfileDetails(
                original.getId(), original.getAccountId(), original.getVin(), /*original.getName(),*/ original.getStatus());

        profile.setSettings(settingsTranslator.translate(original.getNonportableSettings()));
        profile.setApplicationsInstalled(appsTranslator.translate(original.getApplicationsInstalled()));

        return profile;
    }
}
