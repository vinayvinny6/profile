package com.gm.gsmc.pix.profiles.client.local;

import com.gm.gsmc.pix.api.service.VehiclesService;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name = "vehicles", url = "https://vehicles-development.apps.pcfepgwi.gm.com/")
public interface LocalVehicleClient extends VehiclesService {
}