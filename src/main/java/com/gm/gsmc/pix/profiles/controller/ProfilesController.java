package com.gm.gsmc.pix.profiles.controller;

import com.gm.gsmc.pix.api.service.ProfilesService;
import com.gm.gsmc.pix.model.appcatalog.Application;
import com.gm.gsmc.pix.model.profile.ProfileDetails;
import com.gm.gsmc.pix.model.profile.ProfileStatus;
import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import com.gm.gsmc.pix.profiles.jpa.model.Profile;
import com.gm.gsmc.pix.profiles.manager.ProfileAppsManager;
import com.gm.gsmc.pix.profiles.manager.ProfileSettingsManager;
import com.gm.gsmc.pix.profiles.manager.ProfilesManager;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
@Api(description = "Profiles service - Manages the Vehicle Profiles and the Account to Vehicle Mapping")
public class ProfilesController implements ProfilesService {

    private final ProfilesManager profilesManager;
    private final ProfileSettingsManager settingsManager;
    private final ProfileAppsManager appsManager;

    @Autowired
    public ProfilesController(ProfilesManager profilesManager, ProfileSettingsManager settingsManager, ProfileAppsManager appsManager) {
        this.profilesManager = profilesManager;
        this.settingsManager = settingsManager;
        this.appsManager = appsManager;
    }

    // FOR TESTING PURPOSES
    @GetMapping (value = "profiles", produces = { "application/json" })
    public Iterable<Profile> getAllProfiles() {

        return profilesManager.getAllProfiles();
    }

    @Override
    @ApiOperation (value  = "Get a profile ID", notes = "Returns a profile ID for a specific vehicle profile (Account Id + VIN)" )
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The profile ID or VIN # is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value = "accounts/{accountId}/{vin}", produces = {"application/json"})
    public Long getProfileId(
            @PathVariable ("accountId") @ApiParam(value = "The accountId associated with the profile VIN") Long accountId,
            @PathVariable ("vin") @ApiParam(value = "The VIN # of the vehicle", example = "XXXXXXXXXXXXXXXXX") String vin){

        return profilesManager.getProfileId(accountId, vin);
    }

    @Override
    @ApiOperation(value = "Get the all active profiles associated with an account", notes = "Returns a list of profile details (vin, accountId, status, settings, and installed apps) for a specific account")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The profile ID is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value = "accounts/{accountId}/profiles", produces = { "application/json" })
    public List<ProfileDetails> getAccountProfiles(
            @PathVariable @ApiParam(value = "The accountId associated with the VIN", required = true) Long accountId) {

        return profilesManager.getAccountActiveProfiles(accountId);
    }

    @Override
    @ApiOperation(value = "Get the all active profiles associated with a vehicle (VIN)", notes = "Returns a list of profile details (vin, accountId, status, settings, and installed apps) for a specific vin")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The profile ID is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping(value = "/vehicles/{vin}/profiles")
    public List<ProfileDetails> getVehicleProfiles(
            @PathVariable("vin") @ApiParam(value = "The VIN # of the vehicle", required = true) String vin) {

        return profilesManager.getVehicleActiveProfiles(vin);
    }

    @Override
    @ApiOperation (value = "Creates a new vehicle profile", notes = "Returns the profile details of the newly created vehicle profile")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The profile ID or VIN # is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @PostMapping (value = "profiles", produces = { "application/json" })
    public ProfileDetails createProfile(
            @RequestParam("accountId") @ApiParam(value = "The accountId associated with the VIN", required = true) Long accountId,
            @RequestParam("vin") @ApiParam(value = "The VIN # of the vehicle", required = true) String vin,
            @RequestParam(value = "isFirstTimeUser", required = false, defaultValue = "false") @ApiParam(value = "Is this the first profile created under account?") boolean isFirstTime) {

        return profilesManager.createProfile(accountId, vin, isFirstTime);
    }

    @Override
    @ApiOperation(value = "Get the details of a profile", notes = "Returns vin, accountId, status, settings, and installed apps a specific vehicle profile")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The profile ID is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value = "profiles/{profileId}", produces = { "application/json" })
    public ProfileDetails getProfileDetails(
            @PathVariable ("profileId") @ApiParam(value = "The profileId of a (VIN) vehicle profile") Long profileId) {

        return profilesManager.getProfileDetails(profileId);
    }

    @Override
    @ApiOperation(value = "Get the status of a profile", notes = "Returns the status of a specific (VIN) vehicle profile")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The profile ID is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value = "profiles/{profileId}/status", produces = { "application/json" })
    public ProfileStatus getProfileStatus(
            @PathVariable ("profileId") @ApiParam(value = "The profileId of a (VIN) vehicle profile") Long profileId) {

        return profilesManager.getProfileStatus(profileId);
    }

    @Override
    @ApiOperation(value = "Modify the profile status; Activate a (VIN) vehicle profile", notes = "Updates the status of a specific vehicle profile to 'Active'")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The profile ID is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value = "profiles/{profileId}/activate", produces = { "application/json" })
    public ProfileDetails activateProfile(
            @PathVariable("profileId") @ApiParam(value = "The profileId of a (VIN) vehicle profile" ) Long profileId) {

        return profilesManager.activateProfile(profileId);
    }

    @Override
    @ApiOperation(value = "Modify the profile status; Deactivate a (VIN) vehicle profile", notes = "Updates the status of a specific vehicle profile to 'VehicleRemoved'"  )
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The profile ID is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value = "profiles/{profileId}/deactivate", produces = { "application/json" })
    public ProfileDetails deactivateProfile(
            @PathVariable ("profileId") @ApiParam(value = "The profileId of a (VIN) vehicle profile" ) Long profileId) {

        return profilesManager.deactivateProfile(profileId);
    }

    @Override
    @ApiOperation(value = "Get a list of profile settings", notes = "Returns the list of settings of a specific (VIN) vehicle profile")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The profile ID is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value = "profiles/{profileId}/settings", produces = { "application/json" })
    public List<SettingDescriptor> getProfileSettings(
            @PathVariable ("profileId") @ApiParam(value = "The profileId of a (VIN) vehicle profile" ) Long profileId) {

        return settingsManager.getProfileSettings(profileId);
    }

    @Override
    @ApiOperation(value = "Modify a profile's list of settings", notes = "Updates list of settings for a specific (VIN) vehicle profile" )
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The profile ID is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @PostMapping (value = "profiles/{profileId}/settings", produces = { "application/json" })
    public boolean setProfileSettings(
            @PathVariable ("profileId") @ApiParam(value = "The profileId of a (VIN) vehicle profile" ) Long profileId,
            @RequestBody @ApiParam(value = "List of profile settings", required = true) List<SettingDescriptor> settings) {

        return settingsManager.setProfileSettings(profileId, settings);
    }

    @Override
    @ApiOperation(value = "Reset profile settings to default", notes = "Resets list of settings for a specific (VIN) vehicle profile updated to default values  ")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The profile ID or is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @DeleteMapping(value = "profiles/{profileId}/settings", produces = { "application/json" })
    public List<SettingDescriptor> resetProfileSettings(
            @PathVariable("profileId") @ApiParam(value = "The profileId of a (VIN) vehicle profile" ) Long profileId) {

        return settingsManager.resetProfileSettings(profileId);
    }

    @Override
    @ApiOperation(value = "Get a list of installed applications", notes = "Returns a list of installed applications for a specific (VIN) vehicle profile")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The profile ID is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping(value = "profiles/{profileId}/applications", produces = { "application/json" })
    public List<Application> getAppsInstalled(
            @PathVariable("profileId") @ApiParam(value = "The profileId of a (VIN) vehicle profile" ) Long profileId) {

        return appsManager.getProfileAppsInstalled(profileId);
    }

    @Override
    @ApiOperation(value = "Adds new application to a profile", notes ="Adds new application to the list of installed applications of a specific (VIN) vehicle profile" )
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The profile or app ID is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @PostMapping(value = "profiles/{profileId}/applications", produces = { "application/json" })
    public List<Application> addProfileApp(
            @PathVariable ("profileId") @ApiParam(value = "The profileId of a (VIN) vehicle profile" ) Long profileId,
            @RequestParam("appVersionId")  @ApiParam(value = "The ID of an application", required = true ) Long appVersionId) {

        return appsManager.addProfileApp(profileId, appVersionId);
    }

    @Override
    @ApiOperation(value ="Remove an application from a  profile", notes = "Removes an application from the list of installed applications of a specific (VIN) vehicle profile")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The profile or app ID is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @DeleteMapping(value = "profiles/{profileId}/applications/{appVersionId}", produces = { "application/json" })
    public List<Application> removeProfileApp
            (@PathVariable ("profileId") @ApiParam(value = "The profileId of a (VIN) vehicle profile" ) Long profileId,
             @PathVariable ("appVersionId") @ApiParam(value = "The ID of an application", required = true ) Long appVersionId)  {

        return appsManager.removeProfileApp(profileId, appVersionId);
    }

    @Override
    @ApiOperation(value ="Remove all applications from a  profile", notes = "Removes all applications from the list of installed applications of a specific (VIN) vehicle profile")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The profile ID is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @DeleteMapping(value = "profiles/{profileId}/applications", produces = { "application/json" })
    public boolean removeAllInstalledApps(
            @PathVariable ("profileId") @ApiParam(value = "The profileId of a (VIN) vehicle profile" ) Long profileId) {

        return appsManager.removeAllProfileApps(profileId);
    }
}
