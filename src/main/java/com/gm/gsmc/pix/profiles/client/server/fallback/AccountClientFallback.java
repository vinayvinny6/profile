package com.gm.gsmc.pix.profiles.client.server.fallback;

import com.gm.gsmc.pix.model.LanguageCode;
import com.gm.gsmc.pix.model.account.AccountDeleteRequest;
import com.gm.gsmc.pix.model.account.AccountDetails;
import com.gm.gsmc.pix.model.account.AccountStatus;
import com.gm.gsmc.pix.model.account.UserDetails;
import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import com.gm.gsmc.pix.profiles.client.server.AccountClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class AccountClientFallback implements AccountClient {

    private static final Logger log = LoggerFactory.getLogger(AccountClientFallback.class);

    @Override
    public AccountDetails getAccount(String email) {
        AccountDetails account = new AccountDetails();
        account.setStatus(AccountStatus.Active);
        account.setUserDetails(new UserDetails());
        account.getUserDetails().setFirstName("search");
        account.getUserDetails().setLastName("email");
        account.getUserDetails().setLanguage(LanguageCode.en);
        return account;
    }

    @Override
    public AccountDetails getAccount(Long accountId) {
        AccountDetails account = new AccountDetails();
        account.setStatus(AccountStatus.Active);
        account.setUserDetails(new UserDetails());
        account.getUserDetails().setFirstName("get");
        account.getUserDetails().setLastName("account");
        account.getUserDetails().setLanguage(LanguageCode.en);
        return account;
    }

    @Override
    public boolean closeAccount(AccountDeleteRequest accountDeleteRequest) {
        return false;
    }

    @Override
    public UserDetails getUserDetails(Long accountId) {
        log.warn("Unable to get account information, generating an empty user");

        UserDetails user = new UserDetails();
        user.setLanguage(LanguageCode.en);
        return user;
    }

    @Override
    public UserDetails updateUserDetails(Long accountId, UserDetails userDetails) {
        return null;
    }

    @Override
    public AccountStatus getAccountStatus(Long accountId) {
        return null;
    }

    @Override
    public List<SettingDescriptor> getAccountSettings(Long accountId) {
        return null;
    }

    @Override
    public List<SettingDescriptor> updateAccountSettings(Long accountId, List<SettingDescriptor> settings) {
        return null;
    }

    @Override
    public List<Long> getAccountApps(Long accountId) {
        return Arrays.asList(1L, 2L, 3L);
    }

    @Override
    public AccountDetails createAccount(UserDetails userDetails, String vin) {
        return null;
    }

    @Override
    public List<Long> addAccountApp(Long accountId, Long appId) {
        log.warn("Unable to get add app to account, generating an empty list");
        return new ArrayList<>();
    }

    @Override
    public List<Long> removeAccountApp(Long accountId, Long appId) {
        return null;
    }

}
