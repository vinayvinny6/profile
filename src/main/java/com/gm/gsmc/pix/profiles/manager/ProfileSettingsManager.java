package com.gm.gsmc.pix.profiles.manager;

import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import com.gm.gsmc.pix.profiles.client.server.VehicleClient;
import com.gm.gsmc.pix.profiles.exception.ProfileExceptionCode;
import com.gm.gsmc.pix.profiles.jpa.ProfileRepository;
import com.gm.gsmc.pix.profiles.jpa.SettingsRepository;
import com.gm.gsmc.pix.profiles.jpa.model.Profile;
import com.gm.gsmc.pix.profiles.jpa.model.Settings;
import com.gm.gsmc.pix.profiles.translator.ProfileTranslator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProfileSettingsManager {

    private static final Logger log = LoggerFactory.getLogger(ProfileSettingsManager.class);

    private final ProfileRepository profilesRepo;
    private final SettingsRepository settingsRepo;
    private final ProfileTranslator profilesTranslator;
    private final VehicleClient vehicleClient;
    private final ProfileUtility profileUtility;

    @Autowired
    public ProfileSettingsManager(ProfileRepository profilesRepo, SettingsRepository settingsRepo, VehicleClient vehicleClient,
                                  ProfileTranslator profilesTranslator, ProfileUtility profileUtility) {

        this.profilesRepo = profilesRepo;
        this.settingsRepo = settingsRepo;
        this.vehicleClient = vehicleClient;
        this.profilesTranslator = profilesTranslator;
        this.profileUtility = profileUtility;
    }


    public List<SettingDescriptor> getProfileSettings(Long profileId) {
        if (profileId == null) {
            log.error("Invalid Input - profileId is NULL");
            throw ProfileExceptionCode.INVALID_INPUT.exception();
        }

        Profile profile = profileUtility.getProfile(profileId);

        return profilesTranslator.translate(profile).getSettings();
    }


    public boolean setProfileSettings(Long profileId, List<SettingDescriptor> settings) {
        if (profileId == null) {
            log.error("Invalid Input - profileId is NULL");
            throw ProfileExceptionCode.INVALID_INPUT.exception();
        }

        Profile profile = profileUtility.getProfile(profileId);

        replaceProfileSettings(profile, settings);

        return true;
    }


    public List<SettingDescriptor> resetProfileSettings(Long profileId) {
        if (profileId == null) {
            log.error("Invalid Input - profileId is NULL");
            throw ProfileExceptionCode.INVALID_INPUT.exception();
        }

        Profile profile = profileUtility.getProfile(profileId);

        List<SettingDescriptor> settings = vehicleClient.getDefaultVehicleSettings(profile.getVin());

        List<Settings> newSettings = replaceProfileSettings(profile, settings);

        profile.setNonportableSettings(newSettings);
        return profilesTranslator.translate(profile).getSettings();
    }



    @Transactional
    List<Settings> replaceProfileSettings(Profile profile, List<SettingDescriptor> settings){
        //remove old settings
        List<Settings> oldSettings = profile.getNonportableSettings();
        oldSettings.clear();
        profilesRepo.save(profile);

        //create list of new settings
        List<Settings> newSettings = new ArrayList<>();
        Timestamp now = profileUtility.getCurrentTimestamp();

        for (SettingDescriptor setting : settings)
            newSettings.add(new Settings(
                    setting.getCategory(), setting.getName(), setting.getType(), setting.getSelectedValue(), now, profile));

        settingsRepo.save(newSettings);

        return newSettings;
    }
}
