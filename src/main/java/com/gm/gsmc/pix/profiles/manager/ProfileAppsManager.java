package com.gm.gsmc.pix.profiles.manager;

import com.gm.gsmc.pix.model.LanguageCode;
import com.gm.gsmc.pix.model.appcatalog.Application;
import com.gm.gsmc.pix.profiles.client.server.AccountClient;
import com.gm.gsmc.pix.profiles.client.server.AppAvailabilityClient;
import com.gm.gsmc.pix.profiles.client.server.AppCatalogClient;
import com.gm.gsmc.pix.profiles.jpa.AppRepository;
import com.gm.gsmc.pix.profiles.jpa.ProfileRepository;
import com.gm.gsmc.pix.profiles.jpa.model.App;
import com.gm.gsmc.pix.profiles.jpa.model.Profile;
import com.gm.gsmc.pix.profiles.translator.ProfileTranslator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProfileAppsManager {

    private static final Logger log = LoggerFactory.getLogger(ProfileAppsManager.class);

    private final AppRepository appsRepo;
    private final ProfileRepository profilesRepo;
    private final ProfileTranslator profilesTranslator;
    private final AccountClient accountClient;
    private final AppCatalogClient appCatalogClient;
    private final AppAvailabilityClient appAvailClient;
    private final ProfileUtility profileUtility;

    @Autowired
    public ProfileAppsManager(AppRepository appsRepo, ProfileRepository profilesRepo, ProfileTranslator profilesTranslator,
                              AccountClient accountClient, AppAvailabilityClient appAvailClient,
                              AppCatalogClient appCatalogClient, ProfileUtility profileUtility) {

        this.appsRepo = appsRepo;
        this.profilesRepo = profilesRepo;
        this.profilesTranslator = profilesTranslator;
        this.accountClient = accountClient;
        this.appAvailClient = appAvailClient;
        this.appCatalogClient = appCatalogClient;
        this.profileUtility = profileUtility;
    }



    public List<Application> getProfileAppsInstalled(Long profileId) {

        Profile profile = profileUtility.getProfile(profileId);

        //build list of Applications
        List<App> apps = profile.getApplicationsInstalled();
        List<Application> appsInstalled = new ArrayList<>();

        // TODO: 2/27/2018 change getAppVersion to getRequestedAppVersion (to send whole list instead of one by one)
        for (App app: apps)
            if (app.getAppVersionId() != null)
                appsInstalled.add(appCatalogClient.getAppVersion(app.getAppVersionId().intValue()));

        return appsInstalled;
    }

    // use app version id, verify version exists
    public List<Application> addProfileApp(Long profileId, Long appVersionId) {

        Profile profile = profileUtility.getProfile(profileId);

        boolean notInstalled = (appsRepo.findOneByProfileAndAppVersionId(profile, appVersionId) == null);
        if (notInstalled){
            LanguageCode lang = accountClient.getUserDetails(profile.getAccountId()).getLanguage();
            Application app = appAvailClient.getAppVersion(appVersionId.intValue(), profile.getVin(), lang);

            App newApp = new App((long) app.getAppId(), app.getInfo().getName(), (long) app.getVersion().getAppVersionId(), profile);
            profile.getApplicationsInstalled().add(newApp);

            profilesRepo.save(profile);

            // let account know that a new application is installed
            accountClient.addAccountApp(profile.getAccountId(), (long) app.getAppId());
        }

        return profilesTranslator.translate(profile).getApplicationsInstalled();
    }


    public List<Application> removeProfileApp(Long profileId, Long appVersionId)  {

        Profile profile = profileUtility.getProfile(profileId);
        App appToRemove = profileUtility.getAppVersion(appVersionId, profile);

        List<App> appsInstalled =profile.getApplicationsInstalled();
        appsInstalled.remove(appToRemove);

        profilesRepo.save(profile);

        return profilesTranslator.translate(profile).getApplicationsInstalled();
    }


    public boolean removeAllProfileApps(Long profileId) {

        Profile profile = profileUtility.getProfile(profileId);

        List<App> appsInstalled = profile.getApplicationsInstalled();
        appsInstalled.clear();
        profilesRepo.save(profile);

        return true;
    }
}
