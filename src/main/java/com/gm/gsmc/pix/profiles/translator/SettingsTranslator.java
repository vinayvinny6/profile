package com.gm.gsmc.pix.profiles.translator;

import com.gm.gsmc.pix.general.dao.AbstractModelTranslator;
import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import com.gm.gsmc.pix.profiles.jpa.model.Settings;
import org.springframework.stereotype.Service;

@Service
public class SettingsTranslator extends AbstractModelTranslator<Settings, SettingDescriptor> {

    @Override
    public SettingDescriptor translate(Settings original) {
        return new SettingDescriptor(
                original.getCategory(), original.getName(), original.getType(), original.getSelectedValue());
    }
}
