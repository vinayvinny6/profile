package com.gm.gsmc.pix.profiles.config;

import org.springframework.context.annotation.Configuration;

import com.gm.gsmc.pix.general.config.AbstractSwaggerBasicConfiguration;

/**
 * @author TZVZCY
 */
@Configuration
public class SwaggerConfiguration extends AbstractSwaggerBasicConfiguration {

    @Override
    protected String getName() {
        return "Profiles";
    }

    @Override
    protected String getDescription() {
        return "Profiles Service";
    }

    @Override
    protected String getControllerBasePackage() {
        return "com.gm.gsmc.pix.profiles.controller";
    }

}
