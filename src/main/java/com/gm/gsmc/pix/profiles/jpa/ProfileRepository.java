package com.gm.gsmc.pix.profiles.jpa;

import com.gm.gsmc.pix.model.profile.ProfileStatus;
import com.gm.gsmc.pix.profiles.jpa.model.Profile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProfileRepository extends CrudRepository<Profile, Long> {

    List<Profile> findAllByAccountId(Long accountId);

    List<Profile> findAllByAccountIdAndStatus(Long accountId, ProfileStatus status);

    List<Profile> findAllByVinAndStatus(String vin, ProfileStatus status);

    List<Profile> findAllByVinIgnoreCase(String vin);

    Profile findOneByAccountIdAndVinIgnoreCase(@Param("accountId") Long accountId, @Param("vin") String vin);

}

