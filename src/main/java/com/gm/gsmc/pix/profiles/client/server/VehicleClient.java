package com.gm.gsmc.pix.profiles.client.server;

import com.gm.gsmc.pix.api.service.VehiclesService;
import com.gm.gsmc.pix.profiles.client.server.fallback.VehicleClientFallback;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.context.annotation.Primary;

@Primary
@FeignClient(name = "vehicles", fallback = VehicleClientFallback.class)
public interface VehicleClient extends VehiclesService {
}