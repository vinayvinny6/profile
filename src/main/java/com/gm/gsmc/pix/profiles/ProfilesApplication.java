package com.gm.gsmc.pix.profiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.cloud.cloudfoundry.CloudFoundryConnector;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan (basePackages = "com.gm.gsmc.pix.profiles")
@EnableAutoConfiguration(exclude = { RabbitAutoConfiguration.class })
public class ProfilesApplication {

    private static final Logger log = LoggerFactory.getLogger(ProfilesApplication.class);

    public static void main(String[] args) {
        if (new CloudFoundryConnector().isInMatchingCloud()) {
            log.info("Setting Profile to 'CLOUD' Configuration");
            System.setProperty("spring.profiles.active", "cloud");
        }
        SpringApplication.run(ProfilesApplication.class, args);
    }
}
