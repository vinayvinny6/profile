package com.gm.gsmc.pix.profiles.jpa.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.gm.gsmc.pix.model.settings.SettingCategory;
import com.gm.gsmc.pix.model.settings.SettingType;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity(name = "Profile_Settings")
public class Settings {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @Enumerated (EnumType.STRING)
    private SettingCategory category;
    private String name;
    @Enumerated (EnumType.STRING)
    private SettingType type;
    private String selectedValue;
    private Timestamp timestamp;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="PROFILE_ID")
    @JsonBackReference
    private Profile profile;

    public Settings() {
    }

    public Settings(SettingCategory category, String name, SettingType type, String selectedValue, Timestamp timestamp, Profile profile) {
        this.category = category;
        this.name = name;
        this.type = type;
        this.selectedValue = selectedValue;
        this.timestamp = timestamp;
        this.profile = profile;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SettingCategory getCategory() {
        return category;
    }

    public void setCategory(SettingCategory category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SettingType getType() {
        return type;
    }

    public String getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(String selectedValue) {
        this.selectedValue = selectedValue;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profileId) {
        this.profile = profileId;
    }

    @Override
    public String toString(){
        return "ID:" + id + "    NAME:" + name + "    VALUE:" + selectedValue;
    }
}
