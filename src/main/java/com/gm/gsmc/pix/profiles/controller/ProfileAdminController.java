package com.gm.gsmc.pix.profiles.controller;

import com.gm.gsmc.pix.api.admin.service.ProfileAdminService;
import com.gm.gsmc.pix.model.profile.ProfileDetails;
import com.gm.gsmc.pix.profiles.manager.ProfilesManager;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/advisor")
@Api(description = "Advisor UI")
public class ProfileAdminController implements ProfileAdminService {

    private final ProfilesManager profilesManager;

    @Autowired
    public ProfileAdminController(ProfilesManager profilesManager) {
        this.profilesManager = profilesManager;
    }


    @Override
    @ApiOperation(value = "Get the all profiles associated with an account", notes = "Returns a list of profile details (vin, accountId, status, settings, and installed apps) for a specific account")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The account ID is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping(value = "accounts/{accountId}/profiles", produces = { "application/json" })
    public List<ProfileDetails> getAccountProfiles(
            @PathVariable @ApiParam(value = "The accountId associated with the VIN", required = true) Long accountId) {

        return profilesManager.getAccountProfiles(accountId);
    }


    @Override
    @ApiOperation(value = "Close all profiles associated with an account", notes = "For all closed profiles, all settings and installed apps will be removed, status of profile will be updated to AuthenticatedRemoved")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The account ID is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @DeleteMapping (value = "/accounts/{accountId}/profiles")
    public List<ProfileDetails> closeAccountProfiles(@PathVariable Long accountId){
        return profilesManager.closeAccountProfiles(accountId);
    }


    @Override
    @ApiOperation(value = "Close all profiles associated with a VIN", notes = "For all closed profiles, all settings and installed apps will be removed, status of profile will be updated to AuthenticatedRemoved")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The account ID is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @DeleteMapping (value = "/vehicles/{vin}/profiles")
    public List<ProfileDetails> closeVehicleProfiles(@PathVariable String vin){
        return profilesManager.closeVehicleProfiles(vin);
    }


    @Override
    @ApiOperation(value = "Modify the profile status; Remove/Deactivate (VIN) vehicle profile", notes = "Updates the status of a specific vehicle profile to 'AuthenticatedRemoved' ")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The profile ID is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @DeleteMapping (value = "profiles/{profileId}/advisorRemove", produces = { "application/json" })
    public ProfileDetails authenticatedRemoveProfile(
            @PathVariable ("profileId") @ApiParam(value = "The profileId of a (VIN) vehicle profile" ) Long profileId) {

        return profilesManager.advisorRemoveProfile(profileId);
    }

}
