package com.gm.gsmc.pix.profiles.jpa;

import com.gm.gsmc.pix.profiles.jpa.model.App;
import com.gm.gsmc.pix.profiles.jpa.model.Profile;
import org.springframework.data.repository.CrudRepository;

public interface AppRepository extends CrudRepository<App, Long> {

    App findOneByProfileAndAppVersionId(Profile profile, Long appVersionId);

}
