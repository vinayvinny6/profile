package com.gm.gsmc.pix.profiles.jpa;

import com.gm.gsmc.pix.profiles.jpa.model.Settings;
import org.springframework.data.repository.CrudRepository;

public interface SettingsRepository extends CrudRepository<Settings, Long> {
}
