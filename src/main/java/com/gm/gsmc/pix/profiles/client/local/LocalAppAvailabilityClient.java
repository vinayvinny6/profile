package com.gm.gsmc.pix.profiles.client.local;

import com.gm.gsmc.pix.api.service.AppAvailabilityService;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name = "appAvailability", url = "https://availability-development.apps.pcfepgwi.gm.com/")
public interface LocalAppAvailabilityClient extends AppAvailabilityService {
}