package com.gm.gsmc.pix.profiles.client.server;

import com.gm.gsmc.pix.api.service.AppAvailabilityService;
import com.gm.gsmc.pix.profiles.client.server.fallback.AppAvailabilityClientFallback;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.context.annotation.Primary;

@Primary
@FeignClient(name = "appAvailability", fallback = AppAvailabilityClientFallback.class)
public interface AppAvailabilityClient extends AppAvailabilityService {
}