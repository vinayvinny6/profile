package com.gm.gsmc.pix.profiles.exception;

public enum ProfileExceptionCode {

    INVALID_INPUT("0001", "Invalid Input"),
    PROFILE_NOT_FOUND("002", "Profile not found"),
    APP_NOT_FOUND("003", "Application not found"),
    ACCOUNT_NOT_FOUND("004", "Account not found"),
    VEHICLE_NOT_FOUND("005", "Vehicle not found");

    private static final String prefix = "PRFS";
    private final String code;
    private final String description;

    ProfileExceptionCode(String code, String description) {
        this.code = prefix + "_" + code;
        this.description = description;
    }

    public String getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }

    public ProfileException exception(){
        throw new ProfileException(this);
    }
}
