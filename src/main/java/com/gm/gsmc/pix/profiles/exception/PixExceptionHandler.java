package com.gm.gsmc.pix.profiles.exception;

import com.gm.gsmc.pix.general.controller.AbstractRestReponseExceptionHandler;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class PixExceptionHandler extends AbstractRestReponseExceptionHandler {

}
