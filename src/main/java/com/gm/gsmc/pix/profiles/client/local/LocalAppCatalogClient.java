package com.gm.gsmc.pix.profiles.client.local;

import com.gm.gsmc.pix.api.service.AppCatalogService;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient (name = "appCatalog", url = "https://catalog-development.apps.pcfepgwi.gm.com/")
public interface LocalAppCatalogClient extends AppCatalogService {
}