package com.gm.gsmc.pix.profiles.client.server.fallback;

import com.gm.gsmc.pix.model.appcatalog.*;
import com.gm.gsmc.pix.model.vehicle.InfotainmentInterface;
import com.gm.gsmc.pix.profiles.client.server.AppCatalogClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AppCatalogClientFallback implements AppCatalogClient {

    private static final Logger log = LoggerFactory.getLogger(AppCatalogClientFallback.class);

    @Override
    public List<Application> getAllAppsAndVersions(AppType appType) {
        return null;
    }

    @Override
    public List<Application> getAppAllVersions(int appId) {
        return null;
    }

    @Override
    public List<Application> getRequestedAppsAllVersions(List<Integer> appIds) {
        return null;
    }

    @Override
    public Application getAppVersion(int appVersionId) {
        log.warn("Unable to get Application info for appVersionId:{}, generating a DUMMY app", appVersionId);

        Application app = new Application(99, new AppInfo(AppType.CONSUMER, InfotainmentInterface.INFO3, "Fallback app", "AppAvailabilityClient", "Cloud"));
        AppVersion version = new AppVersion(99, 99, 1, 1, 2, "", "", AppVersionStatus.ACTIVE, "");
        version.setAppVersionId(appVersionId);
        app.setVersion(version);

        return app;
    }

    @Override
    public List<AppCategory> getAllCategories() {
        return null;
    }

    @Override
    public List<CategoryApps> getAllCategoriesAndApps() {
        return null;
    }

    @Override
    public CategoryApps getCategoryAndApps(AppCategory category) {
        return null;
    }

    @Override
    public List<AppVersion> getRequestedVersions(List<Integer> versionIds) {
        return new ArrayList<>();
    }
}
