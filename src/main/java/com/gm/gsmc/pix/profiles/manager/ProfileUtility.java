package com.gm.gsmc.pix.profiles.manager;

import com.gm.gsmc.pix.model.account.AccountDetails;
import com.gm.gsmc.pix.model.vehicle.Vehicle;
import com.gm.gsmc.pix.profiles.client.server.AccountClient;
import com.gm.gsmc.pix.profiles.client.server.VehicleClient;
import com.gm.gsmc.pix.profiles.exception.ProfileExceptionCode;
import com.gm.gsmc.pix.profiles.jpa.AppRepository;
import com.gm.gsmc.pix.profiles.jpa.ProfileRepository;
import com.gm.gsmc.pix.profiles.jpa.model.App;
import com.gm.gsmc.pix.profiles.jpa.model.Profile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
class ProfileUtility {

    private static final Logger log = LoggerFactory.getLogger(ProfileUtility.class);

    private final ProfileRepository profileRepo;
    private final AppRepository appRepo;
    private final AccountClient accountClient;
    private final VehicleClient vehicleClient;

    @Autowired
    public ProfileUtility(ProfileRepository profileRepo, AppRepository appRepo, AccountClient accountClient, VehicleClient vehicleClient){
        this.profileRepo = profileRepo;
        this.appRepo = appRepo;
        this.accountClient = accountClient;
        this.vehicleClient = vehicleClient;
    }


    protected Profile getProfile(Long profileId){

        if (profileId == null) {
            log.error("Invalid Input - profileId is NULL");
            throw ProfileExceptionCode.INVALID_INPUT.exception();
        }

        Profile profile = profileRepo.findOne(profileId);

        if (profile == null) {
            log.warn("Not Found - No profile with Id '" + profileId +"'");
            throw ProfileExceptionCode.PROFILE_NOT_FOUND.exception();
        }

        return profile;
    }

    protected App getAppVersion(Long appVersionId, Profile profile){

        if (appVersionId == null || profile == null) {
            if (appVersionId == null) log.error("Invalid Input - appVersionId is NULL");
            if (profile == null) log.error("Invalid Input - profile is NULL");
            throw ProfileExceptionCode.INVALID_INPUT.exception();
        }

        App app = appRepo.findOneByProfileAndAppVersionId(profile, appVersionId);

        if (app == null) {
            log.warn("Not Found - App Version Id '" + appVersionId+"' is not associated with Profile Id '" + profile.getId() +"'");
            throw ProfileExceptionCode.APP_NOT_FOUND.exception();
        }

        return app;
    }

    protected AccountDetails getAccount(Long accountId){

        if (accountId == null) {
            log.error("Invalid Input - accountId is NULL");
            throw ProfileExceptionCode.INVALID_INPUT.exception();
        }

        AccountDetails account = accountClient.getAccount(accountId);

        if (account == null) {
            log.warn("Not Found - No account with Id '" + accountId + "'");
            throw ProfileExceptionCode.ACCOUNT_NOT_FOUND.exception();
        }

        return account;
    }

    protected Vehicle getVehicle(String vin){

        if (vin == null) {
            log.error("Invalid Input - vin is NULL");
            throw ProfileExceptionCode.INVALID_INPUT.exception();
        }

        Vehicle vehicle = vehicleClient.getVehicle(vin);

        if (vehicle == null) {
            log.warn("Not Found - No vehicle with vin '" + vin + "'");
            throw ProfileExceptionCode.VEHICLE_NOT_FOUND.exception();
        }

        return vehicle;
    }

    protected Timestamp getCurrentTimestamp(){
        return new Timestamp(System.currentTimeMillis());
    }
}
