package com.gm.gsmc.pix.profiles.client.server;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.context.annotation.Primary;

import com.gm.gsmc.pix.api.service.AppCatalogService;
import com.gm.gsmc.pix.profiles.client.server.fallback.AppCatalogClientFallback;

@Primary
@FeignClient (name = "appCatalog", fallback = AppCatalogClientFallback.class) // path = "/catalog"
public interface AppCatalogClient extends AppCatalogService {
}