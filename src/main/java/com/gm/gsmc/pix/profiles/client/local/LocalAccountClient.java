package com.gm.gsmc.pix.profiles.client.local;

import com.gm.gsmc.pix.api.service.AccountsService;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name = "accounts", url = "https://accounts-development.apps.pcfepgwi.gm.com/")
public interface LocalAccountClient extends AccountsService{
}
