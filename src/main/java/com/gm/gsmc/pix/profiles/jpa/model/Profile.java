package com.gm.gsmc.pix.profiles.jpa.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.gm.gsmc.pix.model.profile.ProfileStatus;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Profile")
public class Profile {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    private Long accountId;
    private String vin;
    /*private String name;*/

    @Enumerated (EnumType.STRING)
    private ProfileStatus status;

    @OneToMany(mappedBy = "profile", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<Settings> nonportableSettings;

    @OneToMany(mappedBy = "profile", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<App> applicationsInstalled;


    public Profile() {
        nonportableSettings = new ArrayList<>();
        applicationsInstalled = new ArrayList<>();
    }

    public Profile(Long accountId, String vin, /*String name,*/ ProfileStatus status) {
        this.accountId = accountId;
        this.vin = vin.toUpperCase();
        /*this.name = name;*/
        this.status = status;
        nonportableSettings = new ArrayList<>();
        applicationsInstalled = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin.toUpperCase();
    }

    /*public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }*/

    public ProfileStatus getStatus() {
        return status;
    }

    public void setStatus(ProfileStatus status) {
        this.status = status;
    }

    public List<Settings> getNonportableSettings() {
        return nonportableSettings;
    }

    public void setNonportableSettings(List<Settings> nonportableSettings) {
        this.nonportableSettings = nonportableSettings;
    }

    public List<App> getApplicationsInstalled() {
        return applicationsInstalled;
    }

    public void setApplicationsInstalled(List<App> applicationsInstalled) {
        this.applicationsInstalled = applicationsInstalled;
    }

    @Override
    public String toString(){
        return "PROFILE ID:" + id + "    ACCT ID:" + accountId + "    VIN:" + vin + "    STATUS:" + status;
    }
}
