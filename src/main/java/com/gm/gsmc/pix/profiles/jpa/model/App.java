package com.gm.gsmc.pix.profiles.jpa.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity(name="Profile_Apps")
public class App {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Long extAppId;
    private Long appVersionId;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="PROFILE_ID")
    @JsonBackReference
    private Profile profile;

    public App() { }

    public App(Long extAppId, String name, Long appVersionId, Profile profile) {
        this.extAppId = extAppId;
        this.name = name;
        this.appVersionId = appVersionId;
        this.profile = profile;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExtAppId() {
        return extAppId;
    }

    public void setExtAppId(Long extAppId) {
        this.extAppId = extAppId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getAppVersionId() {
        return appVersionId;
    }

    public void setAppVersionId(Long appVersionId) {
        this.appVersionId = appVersionId;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    @Override
    public String toString(){
        return "APP ID:" + id + "    NAME:" + name + "    VERSION ID:" + appVersionId;
    }
}
