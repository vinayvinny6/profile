package com.gm.gsmc.pix.profiles.translator;

import com.gm.gsmc.pix.general.dao.AbstractModelTranslator;
import com.gm.gsmc.pix.model.appcatalog.AppVersion;
import com.gm.gsmc.pix.model.appcatalog.Application;
import com.gm.gsmc.pix.profiles.client.server.AppCatalogClient;
import com.gm.gsmc.pix.profiles.jpa.model.App;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AppTranslator extends AbstractModelTranslator<App, Application> {

    private final AppCatalogClient appCatalogClient;

    @Autowired
    public AppTranslator(AppCatalogClient appCatalogClient) {
        this.appCatalogClient = appCatalogClient;
    }

    @Override
    public Application translate(App original) {
        return appCatalogClient.getAppVersion(original.getAppVersionId().intValue());
    }

    public List<AppVersion> translateList(List<App> originalAppList){
        List<Integer> appVersionIds = new ArrayList<>();
        originalAppList.forEach(app -> appVersionIds.add(app.getAppVersionId().intValue()));

        return appCatalogClient.getRequestedVersions(appVersionIds);
    }
}
