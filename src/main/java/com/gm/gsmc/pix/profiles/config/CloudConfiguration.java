package com.gm.gsmc.pix.profiles.config;

import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.java.AbstractCloudConfig;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("cloud")
@EnableDiscoveryClient
@EnableFeignClients (basePackages = "com.gm.gsmc.pix.profiles.client.server")
@EnableCircuitBreaker
public class CloudConfiguration extends AbstractCloudConfig {
}
