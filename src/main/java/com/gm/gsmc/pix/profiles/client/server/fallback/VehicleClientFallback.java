package com.gm.gsmc.pix.profiles.client.server.fallback;

import com.gm.gsmc.pix.model.CountryIsoCode;
import com.gm.gsmc.pix.model.settings.SettingCategory;
import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import com.gm.gsmc.pix.model.settings.SettingType;
import com.gm.gsmc.pix.model.vehicle.Vehicle;
import com.gm.gsmc.pix.model.vehicle.VehicleCapability;
import com.gm.gsmc.pix.model.vehicle.VehicleState;
import com.gm.gsmc.pix.profiles.client.server.VehicleClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class VehicleClientFallback implements VehicleClient{

    private static final Logger log = LoggerFactory.getLogger(VehicleClientFallback.class);


    @Override
    public Vehicle createVehicle(Vehicle vehicle) {
        return null;
    }

    @Override
    public Vehicle getVehicle(String vin) {
        return new Vehicle();
    }

    @Override
    public CountryIsoCode updateVehicleCountry(String vin, CountryIsoCode countryIsoCode) {
        return null;
    }

    @Override
    public CountryIsoCode getVehicleCountry(String vin) {
        return null;
    }

    @Override
    public List<VehicleCapability> getVehicleCapabilities(String vin) {
        return null;
    }

    @Override
    public List<SettingDescriptor> getDefaultVehicleSettings(String vin) {
        log.warn("Unable to get default vehicle settings, generating a DUMMY list");

        List<SettingDescriptor> settings = new ArrayList<>();
        settings.add(new SettingDescriptor(SettingCategory.VehicleSetting, "HELLO", SettingType.STRING, "WORLD!"));

        return settings;
    }

    @Override
    public List<VehicleCapability> setVehicleCapabilities(String vin, List<VehicleCapability> vehicleCapabilities) {
        return null;
    }

    @Override
    public Vehicle updateVehicleState(String vin, VehicleState vehicleState) {
        return null;
    }
}
