package com.gm.gsmc.pix.profiles.client.server.fallback;

import com.gm.gsmc.pix.model.LanguageCode;
import com.gm.gsmc.pix.model.appcatalog.*;
import com.gm.gsmc.pix.model.vehicle.InfotainmentInterface;
import com.gm.gsmc.pix.profiles.client.server.AppAvailabilityClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AppAvailabilityClientFallback implements AppAvailabilityClient {

    private static final Logger log = LoggerFactory.getLogger(AppAvailabilityClientFallback.class);


    @Override
    public List<Application> getApps(String vin, LanguageCode language, AppType appType) {
        log.warn("Unable to get Applications, generating a DUMMY LIST app");
        List<Application> apps = new ArrayList<>();
        Application app;
        AppVersion version;

        if (appType.equals(AppType.CONSUMER)) {
            app = new Application(99, new AppInfo(AppType.CONSUMER, InfotainmentInterface.INFO3, "Fallback app", "AppAvailabilityClient", "Cloud"));
            version = new AppVersion(99, 99, 1, 1, 2, "", "", AppVersionStatus.ACTIVE, "");
            app.setVersion(version);
            apps.add(app);
        }

        if (appType.equals(AppType.CORE)) {
            app = new Application(88, new AppInfo(AppType.CORE, InfotainmentInterface.INFO3, "Fallback app2", "AppAvailabilityClient", "Cloud"));
            version = new AppVersion(88, 88, 1, 1, 2, "", "", AppVersionStatus.ACTIVE, "");
            app.setVersion(version);
            apps.add(app);
        }

        return apps;
    }

    @Override
    public Application getAppLatestVersion(int appId, String vin, LanguageCode language) {
        return null;
    }

    @Override
    public Application getAppVersion(int appVersionId, String vin, LanguageCode language) {
        log.warn("Unable to get Application, generating a DUMMY app");

        Application app = new Application(99, new AppInfo(AppType.CONSUMER, InfotainmentInterface.INFO3, "Fallback app", "AppAvailabilityClient", "Cloud"));
        AppVersion version = new AppVersion(99, appVersionId, 1, 1, 2, "", "", AppVersionStatus.ACTIVE, "");
        app.setVersion(version);

        return app;
    }

    @Override
    public List<Application> getRequestedApps(List<Integer> appIds, String vin, LanguageCode language) {
        log.warn("Unable to get Applications, generating a DUMMY LIST app");
        List<Application> apps = new ArrayList<>();
        Application app;
        AppVersion version;

        app = new Application(1, new AppInfo(AppType.CONSUMER, InfotainmentInterface.INFO3, "Starbucks", "Starbucks", "Starbucks"));
        version = new AppVersion(1, 1, 1, 1, 1, "", "", AppVersionStatus.ACTIVE, "");
        app.setVersion(version);
        apps.add(app);

        app = new Application(2, new AppInfo(AppType.CONSUMER, InfotainmentInterface.INFO3, "Weather Channel", "Weather Channel", "Weather Channel"));
        version = new AppVersion(2, 2, 2, 2, 2, "", "", AppVersionStatus.ACTIVE, "");
        app.setVersion(version);
        apps.add(app);

        app = new Application(3, new AppInfo(AppType.CONSUMER, InfotainmentInterface.INFO3, "Glympse", "Glympse", "Glympse"));
        version = new AppVersion(3, 3, 3, 3, 3, "", "", AppVersionStatus.ACTIVE, "");
        app.setVersion(version);
        apps.add(app);

        return apps;
    }

    @Override
    public List<AppCategory> getAppCategories(String vin, LanguageCode language) {
        return null;
    }

    @Override
    public List<CategoryApps> getCategoriesAndApps(String vin, LanguageCode language) {
        return null;
    }

    @Override
    public List<Application> getAppsForCategory(AppCategory categoryName, String vin, LanguageCode language) {
        return null;
    }
}
