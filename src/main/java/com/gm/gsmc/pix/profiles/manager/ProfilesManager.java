package com.gm.gsmc.pix.profiles.manager;

import com.gm.gsmc.pix.model.LanguageCode;
import com.gm.gsmc.pix.model.account.AccountDetails;
import com.gm.gsmc.pix.model.account.AccountStatus;
import com.gm.gsmc.pix.model.appcatalog.Application;
import com.gm.gsmc.pix.model.profile.ProfileDetails;
import com.gm.gsmc.pix.model.profile.ProfileStatus;
import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import com.gm.gsmc.pix.profiles.client.server.AccountClient;
import com.gm.gsmc.pix.profiles.client.server.AppAvailabilityClient;
import com.gm.gsmc.pix.profiles.client.server.VehicleClient;
import com.gm.gsmc.pix.profiles.exception.ProfileExceptionCode;
import com.gm.gsmc.pix.profiles.jpa.ProfileRepository;
import com.gm.gsmc.pix.profiles.jpa.SettingsRepository;
import com.gm.gsmc.pix.profiles.jpa.model.App;
import com.gm.gsmc.pix.profiles.jpa.model.Profile;
import com.gm.gsmc.pix.profiles.jpa.model.Settings;
import com.gm.gsmc.pix.profiles.translator.ProfileTranslator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ProfilesManager {

    private static final Logger log = LoggerFactory.getLogger(ProfilesManager.class);

    private final ProfileRepository profilesRepo;
    private final ProfileTranslator profilesTranslator;
    private final SettingsRepository settingsRepo;
    private final AccountClient accountClient;
    private final VehicleClient vehicleClient;
    private final AppAvailabilityClient appAvailClient;
    private final ProfileUtility profileUtility;

    @Autowired
    public ProfilesManager(ProfileRepository profilesRepo, ProfileTranslator profilesTranslator,
                           SettingsRepository settingsRepo, AccountClient accountClient, VehicleClient vehicleClient,
                           AppAvailabilityClient appAvailClient, ProfileUtility profileUtility) {

        this.profilesRepo = profilesRepo;
        this.profilesTranslator = profilesTranslator;
        this.settingsRepo = settingsRepo;
        this.accountClient = accountClient;
        this.vehicleClient = vehicleClient;
        this.appAvailClient = appAvailClient;
        this.profileUtility = profileUtility;
    }


    //for testing purposes
    public Iterable<Profile> getAllProfiles() {

        return profilesRepo.findAll();
    }


    public List<ProfileDetails> getAccountProfiles(Long accountId) {
        if (accountId == null) {
            log.error("Invalid Input: Account Id is NULL");
            throw ProfileExceptionCode.INVALID_INPUT.exception();
        }

        List<Profile> profiles = profilesRepo.findAllByAccountId(accountId);
        if (profiles == null)
            return new ArrayList<>();

        return profilesTranslator.translate(profiles);
    }

    public List<ProfileDetails> getAccountActiveProfiles(Long accountId) {
        if (accountId == null) {
            log.error("Invalid Input: Account Id is NULL");
            throw ProfileExceptionCode.INVALID_INPUT.exception();
        }

        List<Profile> profiles = profilesRepo.findAllByAccountIdAndStatus(accountId, ProfileStatus.Active);
        if (profiles == null)
            return new ArrayList<>();

        return profilesTranslator.translate(profiles);
    }

    public List<ProfileDetails> getVehicleActiveProfiles(String vin) {
        if (vin == null){
            log.error("Invalid Input: Vin is NULL");
            throw ProfileExceptionCode.INVALID_INPUT.exception();
        }

        List<Profile> profiles = profilesRepo.findAllByVinAndStatus(vin, ProfileStatus.Active);
        if (profiles == null)
            return new ArrayList<>();

        return profilesTranslator.translate(profiles);
    }


    public Long getProfileId(Long accountId, String vin){
        if (accountId == null || vin == null) {
            StringBuilder errorMsg = new StringBuilder("Invalid Input: ");
            if (accountId == null) errorMsg.append("AccountId is NULL. ");
            if (vin == null) errorMsg.append("Vin is NULL.");

            log.error(errorMsg.toString());
            throw ProfileExceptionCode.INVALID_INPUT.exception();
        }

        Profile profile = profilesRepo.findOneByAccountIdAndVinIgnoreCase(accountId, vin);

        if (profile == null)
            throw ProfileExceptionCode.PROFILE_NOT_FOUND.exception();

        return profile.getId();
    }


    public ProfileDetails getProfileDetails(Long profileId) {

        Profile profile = profileUtility.getProfile(profileId);

        return profilesTranslator.translate(profile);
    }


    public ProfileStatus getProfileStatus(Long profileId) {

        Profile profile = profileUtility.getProfile(profileId);

        return profilesTranslator.translate(profile).getStatus();
    }


    public ProfileDetails activateProfile(Long profileId) {

        try {
            Profile profile = profileUtility.getProfile(profileId);

            profile.setStatus(ProfileStatus.Active);
            profile = profilesRepo.save(profile);

            return profilesTranslator.translate(profile);
        }
        catch (Exception e){
            log.error(e.getMessage());

            return null;
        }
    }


    public ProfileDetails deactivateProfile(Long profileId) {

        try {
            Profile profile = profileUtility.getProfile(profileId);

            profile.setStatus(ProfileStatus.VehicleRemoved);
            profile = profilesRepo.save(profile);

            return profilesTranslator.translate(profile);
        }
        catch (Exception e){
            log.error(e.getMessage());

            return null;
        }
    }


    public ProfileDetails advisorRemoveProfile(Long profileId) {

        try{
            Profile profile = profileUtility.getProfile(profileId);

            profile.setStatus(ProfileStatus.AuthenticatedRemoved);
            profile = profilesRepo.save(profile);

            return profilesTranslator.translate(profile);
        }
        catch (Exception e){
            log.error(e.getMessage());

            return null;
        }
    }

    /* (List of assumptions) For each account profile:
        1. Delete All Non-Portable Settings
        2. Delete List of Apps Installed
        3. Change Profile status to AuthenticatedRemoved
     */
    @Transactional
    public List<ProfileDetails> closeAccountProfiles(Long accountId) {
        if (accountId == null) {
            log.error("Invalid Input: Account Id is NULL");
            throw ProfileExceptionCode.INVALID_INPUT.exception();
        }

        List<Profile> profiles = profilesRepo.findAllByAccountId(accountId);
        if (profiles == null) {
            log.info("No Profiles found under accountId: " + accountId);
            return new ArrayList<>();
        }

        //For each profile
        for (Profile profile : profiles){
            profile.getApplicationsInstalled().clear();
            profile.getNonportableSettings().clear();
            profile.setStatus(ProfileStatus.AuthenticatedRemoved);
        }

        try {
            profilesRepo.save(profiles);
        }
        catch (Exception e){
            log.error(e.getMessage());
            return null;
        }

        return profilesTranslator.translate(profiles);
    }

    /* (List of assumptions) For each vehicle profile:
        1. Delete All Non-Portable Settings
        2. Delete List of Apps Installed
        3. Change Profile status to AuthenticatedRemoved
     */
    @Transactional
    public List<ProfileDetails> closeVehicleProfiles(String vin) {
        if (vin == null || vin.isEmpty()) {
            log.error("Invalid Input: Vin is NULL");
            throw ProfileExceptionCode.INVALID_INPUT.exception();
        }

        List<Profile> profiles = profilesRepo.findAllByVinIgnoreCase(vin);
        if (profiles == null) {
            log.info("No Profiles associated with VIN: " + vin);
            return new ArrayList<>();
        }

        //For each profile
        for (Profile profile : profiles){
            profile.getApplicationsInstalled().clear();
            profile.getNonportableSettings().clear();
            profile.setStatus(ProfileStatus.AuthenticatedRemoved);
        }

        try {
            profilesRepo.save(profiles);
        }
        catch (Exception e){
            log.error(e.getMessage());
            return null;
        }

        return profilesTranslator.translate(profiles);
    }

    /* Steps:
        1.  If profile already exists and is Active, then return.
        1.1 If profile already exists and is NOT Active, set profile to Active and return.
        2.  Special Case, if is first time user, then assume account exists and is active.
        2.1 Otherwise, verify account exists and is Active.
        3.  Vehicle has to exist on VehicleService
        4.  Create Profile
        5.  Initialize Vehicle Default Settings
        6.  If not first time user, cross-check profiles to verify which apps to install and let Notification Manager know
     */
    @Transactional
    public ProfileDetails createProfile(Long accountId, String vin, boolean isFirstTimeUser) {
        // 1. Check if profile exists
        Profile profile;
        Profile existingProfile = profilesRepo.findOneByAccountIdAndVinIgnoreCase(accountId, vin);

        // 1.1 If profile already exists, verify status is set to active and return
        if (existingProfile != null) {
            log.info("Profile already exists, overwriting status to Active.");

            profile = existingProfile;
            profile.setStatus(ProfileStatus.Active);
            profilesRepo.save(profile);

            return profilesTranslator.translate(profile);
        }

        AccountDetails account;
        List<Profile> accountProfiles;

        // 2. If isFirstTimeUser, assume that an empty account exists (no need to verify)
        if (isFirstTimeUser){
            account = new AccountDetails();
            accountProfiles = new ArrayList<>();
        }
        // 2.1 Otherwise Verify Account Exists and that is active, if so, get all active profiles
        else {
            account = profileUtility.getAccount(accountId);
            if(!AccountStatus.Active.equals(account.getStatus())){
                log.error("Account was found, but is not Active.");
                throw ProfileExceptionCode.ACCOUNT_NOT_FOUND.exception();
            }
            accountProfiles = profilesRepo.findAllByAccountIdAndStatus(accountId, ProfileStatus.Active);
            if (accountProfiles == null)
                accountProfiles = new ArrayList<>();
        }

        // 3. Verify Vehicle VIN is valid
        profileUtility.getVehicle(vin);

        // 4. Create the profile
        profile = new Profile(accountId, vin.toUpperCase(), ProfileStatus.Active);
        profilesRepo.save(profile);

        // 5. Get Vehicle Default Settings
        List<SettingDescriptor> defaultSettings = vehicleClient.getDefaultVehicleSettings(vin);
        List<Settings> nonPortableSettings = new ArrayList<>();
        Timestamp now  = profileUtility.getCurrentTimestamp();

        for (SettingDescriptor s : defaultSettings) {
            nonPortableSettings.add(new Settings(s.getCategory(), s.getName(), s.getType(), s.getSelectedValue(), now, null));
        }
        profile.setNonportableSettings(nonPortableSettings);
        settingsRepo.save(nonPortableSettings);


        // 6.  If not first time user, cross-check profiles to verify which apps to install and let Notification Manager know
        if (!isFirstTimeUser) {
            // 6.1 Get Account Apps
            LanguageCode lang = account.getUserDetails().getLanguage();
            List<Integer> appIds = new ArrayList<>();

            // 6.1.1 Converting Long to Integer
            accountClient.getAccountApps(accountId).forEach(appId -> appIds.add(appId.intValue()));

            // 6.2 Call App Avail to get app version to install
            List<Application> consumerApps = appAvailClient.getRequestedApps(appIds, vin, lang);

            // 6.3 Cross-check profiles to verify apps to install
            Set<Application> appsToInstall = new HashSet<>();

            for (Profile otherProfiles : accountProfiles) {
                List<App> appsPerProfile = otherProfiles.getApplicationsInstalled();

                if (!appsPerProfile.isEmpty())
                    for (Application app : consumerApps) {
                        if (appsPerProfile.stream().anyMatch(a -> a.getExtAppId().intValue() == app.getAppId()))
                            appsToInstall.add(app);
                    }
            }

            // 6.4 Let Notifications now which consumer apps to install
            // TODO: 2/26/2018 call notifications with appsToInstall
            //appsToInstall.forEach(app -> System.out.println(app.getAppId()));
        }


        return profilesTranslator.translate(profile);
    }
}
