package com.gm.gsmc.pix.profiles.client.server;

import com.gm.gsmc.pix.api.service.AccountsService;
import com.gm.gsmc.pix.profiles.client.server.fallback.AccountClientFallback;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.context.annotation.Primary;

@Primary
@FeignClient(name = "accounts", fallback = AccountClientFallback.class)
public interface AccountClient extends AccountsService{
}
