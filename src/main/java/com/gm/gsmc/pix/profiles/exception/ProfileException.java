package com.gm.gsmc.pix.profiles.exception;

import com.gm.gsmc.pix.api.service.PixException;

public class ProfileException extends PixException {

    public ProfileException (ProfileExceptionCode code) {
        super(code.getDescription(), code.getCode());
    }

    public ProfileException (ProfileExceptionCode code, Throwable cause) {
        super(code.getDescription(), code.getCode(), cause);
    }
}
