package com.gm.gsmc.pix.profiles.manager;

import com.gm.gsmc.pix.model.profile.ProfileStatus;
import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import com.gm.gsmc.pix.profiles.client.server.VehicleClient;
import com.gm.gsmc.pix.profiles.exception.ProfileException;
import com.gm.gsmc.pix.profiles.exception.ProfileExceptionCode;
import com.gm.gsmc.pix.profiles.jpa.ProfileRepository;
import com.gm.gsmc.pix.profiles.jpa.SettingsRepository;
import com.gm.gsmc.pix.profiles.jpa.model.Profile;
import com.gm.gsmc.pix.profiles.translator.ProfileTranslator;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;

import java.util.List;

import static com.gm.gsmc.pix.profiles.TestData.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("Duplicates")
public class ProfileSettingsManagerTest {

    @InjectMocks
    private ProfileSettingsManager manager;
    @Mock
    private ProfileRepository profileRepo;
    @Mock
    private ProfileUtility profileUtility;
    @Mock
    private SettingsRepository settingsRepo;
    @Mock
    private ProfileTranslator profilesTranslator;
    @Mock
    private VehicleClient vehicleClient;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);

        when(profileUtility.getProfile(1L)).thenReturn(PROFILE_1);
        when(profileUtility.getProfile(2L)).thenReturn(PROFILE_2);
        when(profileUtility.getProfile(3L)).thenReturn(PROFILE_3);
        when(profilesTranslator.translate(PROFILE_1)).thenReturn(PROFILE_DETAILS_1);
        when(profilesTranslator.translate(PROFILE_2)).thenReturn(PROFILE_DETAILS_2);
        when(profilesTranslator.translate(PROFILE_3)).thenReturn(PROFILE_DETAILS_3);



        when(profileRepo.save(any(Profile.class))).thenAnswer((Answer<Profile>) invocation -> {
            Object[] args = invocation.getArguments();
            return (Profile) args[0];
        });
    }

    @Test
    public void getProfileSettings() {
        List<SettingDescriptor> settings = manager.getProfileSettings(1L);
        assertNotNull(settings);
        assertEquals(3, settings.size());


        settings = manager.getProfileSettings(2L);
        assertNotNull(settings);
        assertEquals(3, settings.size());

        try {
            manager.getProfileSettings(null);
        } catch (ProfileException ex){
            assertEquals(ProfileExceptionCode.INVALID_INPUT.getCode(), ex.getReasonCode());
        }
    }

    @Test
    public void resetProfileSettings() {
        List<SettingDescriptor> settings = manager.resetProfileSettings(1L);
        assertNotNull(settings);


        settings = manager.resetProfileSettings(2L);
        assertNotNull(settings);

        try {
            manager.resetProfileSettings(null);
        } catch (ProfileException ex){
            assertEquals(ProfileExceptionCode.INVALID_INPUT.getCode(), ex.getReasonCode());
        }
    }
}