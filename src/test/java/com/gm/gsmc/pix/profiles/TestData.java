package com.gm.gsmc.pix.profiles;


import com.gm.gsmc.pix.model.account.AccountDetails;
import com.gm.gsmc.pix.model.account.UserDetails;
import com.gm.gsmc.pix.model.profile.ProfileDetails;
import com.gm.gsmc.pix.model.profile.ProfileStatus;
import com.gm.gsmc.pix.model.settings.SettingCategory;
import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import com.gm.gsmc.pix.model.settings.SettingType;
import com.gm.gsmc.pix.model.vehicle.Vehicle;
import com.gm.gsmc.pix.profiles.jpa.model.App;
import com.gm.gsmc.pix.profiles.jpa.model.Profile;
import com.gm.gsmc.pix.profiles.jpa.model.Settings;
import com.google.common.collect.Lists;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

public class TestData {

    public static AccountDetails ACCOUNT_1;
    public static AccountDetails ACCOUNT_2;
    public static List<Profile> ACCOUNT_1_PROFILES;
    public static List<Profile> ACCOUNT_2_PROFILES;
    public static List<ProfileDetails> ACCOUNT_1_PROFILE_DETAILS;
    public static List<ProfileDetails> ACCOUNT_2_PROFILE_DETAILS;
    public static Vehicle VEHICLE_1;
    public static Vehicle VEHICLE_2;
    public static List<Profile> VEHICLE_1_PROFILES;
    public static List<Profile> VEHICLE_2_PROFILES;
    public static List<ProfileDetails> VEHICLE_1_PROFILE_DETAILS;
    public static List<ProfileDetails> VEHICLE_2_PROFILE_DETAILS;
    public static Profile PROFILE_1;
    public static Profile PROFILE_2;
    public static Profile PROFILE_3;
    public static ProfileDetails PROFILE_DETAILS_1;
    public static ProfileDetails PROFILE_DETAILS_2;
    public static ProfileDetails PROFILE_DETAILS_3;
    public static App APP_1;
    public static App APP_2;
    public static App APP_3;
    public static App APP_4;
    public static Settings SETTING_1;
    public static Settings SETTING_2;
    public static Settings SETTING_3;
    public static Settings SETTING_4;
    public static Settings SETTING_5;
    public static Settings SETTING_6;
    public static SettingDescriptor SETTING_DESCRIPTOR_1;
    public static SettingDescriptor SETTING_DESCRIPTOR_2;
    public static SettingDescriptor SETTING_DESCRIPTOR_3;
    public static SettingDescriptor SETTING_DESCRIPTOR_4;
    public static SettingDescriptor SETTING_DESCRIPTOR_5;
    public static SettingDescriptor SETTING_DESCRIPTOR_6;

    static {
        UserDetails userDetails = new UserDetails();
        userDetails.setFirstName("Zac");
        userDetails.setLastName("Grantham");
        userDetails.setEmail("zac.grantham@gm.com");
        ACCOUNT_1 = new AccountDetails();
        ACCOUNT_1.setUserDetails(userDetails);
        ACCOUNT_1.setId(65L);

        userDetails = new UserDetails();
        userDetails.setFirstName("Aaron");
        userDetails.setLastName("Johnson");
        userDetails.setEmail("aaron.johnson@gm.com");
        ACCOUNT_2 = new AccountDetails();
        ACCOUNT_2.setUserDetails(userDetails);
        ACCOUNT_2.setId(66L);

        VEHICLE_1 = new Vehicle();
        VEHICLE_1.setVin("VIN111");
        VEHICLE_1.setYear(2018);
        VEHICLE_1.setMake("Chevrolet");
        VEHICLE_1.setModel("Malibu");

        VEHICLE_2 = new Vehicle();
        VEHICLE_2.setVin("VIN222");
        VEHICLE_2.setYear(2018);
        VEHICLE_2.setMake("Cadillac");
        VEHICLE_2.setModel("ATS");

        PROFILE_1 = new Profile(65L, "VIN111", ProfileStatus.Active);
        PROFILE_1.setId(1L);

        PROFILE_2 = new Profile(66L, "VIN222", ProfileStatus.Active);
        PROFILE_2.setId(2L);

        PROFILE_3 = new Profile(66L, "VIN111", ProfileStatus.Active);
        PROFILE_3.setId(3L);

        ACCOUNT_1_PROFILES = Lists.newArrayList(PROFILE_1);
        ACCOUNT_2_PROFILES = Lists.newArrayList(PROFILE_2, PROFILE_3);

        VEHICLE_1_PROFILES = Lists.newArrayList(PROFILE_1, PROFILE_3);
        VEHICLE_2_PROFILES = Lists.newArrayList(PROFILE_2);

        PROFILE_DETAILS_1 = new ProfileDetails(1L, 65L, "VIN111", ProfileStatus.Active);
        PROFILE_DETAILS_2 = new ProfileDetails(2L, 66L, "VIN222", ProfileStatus.Active);
        PROFILE_DETAILS_3 = new ProfileDetails(3L, 66L, "VIN111", ProfileStatus.Active);

        ACCOUNT_1_PROFILE_DETAILS = Collections.singletonList(PROFILE_DETAILS_1);
        ACCOUNT_2_PROFILE_DETAILS = Lists.newArrayList(PROFILE_DETAILS_2, PROFILE_DETAILS_3);

        VEHICLE_1_PROFILE_DETAILS = Lists.newArrayList(PROFILE_DETAILS_1, PROFILE_DETAILS_3);
        VEHICLE_2_PROFILE_DETAILS = Lists.newArrayList(PROFILE_DETAILS_2);

        APP_1 = new App(1L, "Starbucks", 1L, PROFILE_1);
        APP_2 = new App(3L, "Glympse", 3L, PROFILE_1);
        APP_3 = new App(1L, "Starbucks", 1L, PROFILE_2);
        APP_4 = new App(2L, "Tim Hortons", 2L, PROFILE_2);
        APP_1.setId(1L);
        APP_2.setId(2L);
        APP_3.setId(3L);
        APP_4.setId(4L);

        PROFILE_1.setApplicationsInstalled(Lists.newArrayList(APP_1, APP_2));
        PROFILE_2.setApplicationsInstalled(Lists.newArrayList(APP_3, APP_4));

        Timestamp now = new Timestamp(System.currentTimeMillis());
        SETTING_1 = new Settings(SettingCategory.VehicleSetting, "Bass", SettingType.INT, "10", now, PROFILE_1);
        SETTING_2 = new Settings(SettingCategory.VehicleSetting, "Mid", SettingType.INT, "10", now, PROFILE_1);
        SETTING_3 = new Settings(SettingCategory.VehicleSetting, "Treble", SettingType.INT, "10", now, PROFILE_1);
        SETTING_4 = new Settings(SettingCategory.VehicleSetting, "XMRadio1", SettingType.STRING, "Pop2K", now, PROFILE_2);
        SETTING_5 = new Settings(SettingCategory.VehicleSetting, "XMRadio2", SettingType.STRING, "ESPN", now, PROFILE_2);
        SETTING_6 = new Settings(SettingCategory.VehicleSetting, "XMRadio3", SettingType.STRING, "KidzBop", now, PROFILE_2);

        PROFILE_1.setNonportableSettings(Lists.newArrayList(SETTING_1, SETTING_2, SETTING_3));
        PROFILE_2.setNonportableSettings(Lists.newArrayList(SETTING_4, SETTING_5, SETTING_6));

        SETTING_DESCRIPTOR_1 = new SettingDescriptor(SettingCategory.VehicleSetting, "Bass", SettingType.INT, "10");
        SETTING_DESCRIPTOR_2 = new SettingDescriptor(SettingCategory.VehicleSetting, "Mid", SettingType.INT, "10");
        SETTING_DESCRIPTOR_3 = new SettingDescriptor(SettingCategory.VehicleSetting, "Treble", SettingType.INT, "10");
        SETTING_DESCRIPTOR_4 = new SettingDescriptor(SettingCategory.VehicleSetting, "XMRadio1", SettingType.STRING, "Pop2K");
        SETTING_DESCRIPTOR_5 = new SettingDescriptor(SettingCategory.VehicleSetting, "XMRadio2", SettingType.STRING, "ESPN");
        SETTING_DESCRIPTOR_6 = new SettingDescriptor(SettingCategory.VehicleSetting, "XMRadio3", SettingType.STRING, "KidzBop");

        PROFILE_DETAILS_1.setSettings(Lists.newArrayList(SETTING_DESCRIPTOR_1, SETTING_DESCRIPTOR_2, SETTING_DESCRIPTOR_3));
        PROFILE_DETAILS_2.setSettings(Lists.newArrayList(SETTING_DESCRIPTOR_4, SETTING_DESCRIPTOR_5, SETTING_DESCRIPTOR_6));


    }
}
