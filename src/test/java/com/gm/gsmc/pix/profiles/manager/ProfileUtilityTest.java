package com.gm.gsmc.pix.profiles.manager;

import com.gm.gsmc.pix.model.account.AccountDetails;
import com.gm.gsmc.pix.model.vehicle.Vehicle;
import com.gm.gsmc.pix.profiles.client.server.AccountClient;
import com.gm.gsmc.pix.profiles.client.server.VehicleClient;
import com.gm.gsmc.pix.profiles.exception.ProfileException;
import com.gm.gsmc.pix.profiles.exception.ProfileExceptionCode;
import com.gm.gsmc.pix.profiles.jpa.AppRepository;
import com.gm.gsmc.pix.profiles.jpa.ProfileRepository;
import com.gm.gsmc.pix.profiles.jpa.model.App;
import com.gm.gsmc.pix.profiles.jpa.model.Profile;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.gm.gsmc.pix.profiles.TestData.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class ProfileUtilityTest {

    @InjectMocks
    private ProfileUtility profileUtility;
    @Mock
    private ProfileRepository profileRepo;
    @Mock
    private AppRepository appRepo;
    @Mock
    private AccountClient accountClient;
    @Mock
    private VehicleClient vehicleClient;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);

        when(profileRepo.findOne(1L)).thenReturn(PROFILE_1);
        when(profileRepo.findOne(2L)).thenReturn(PROFILE_2);

        when(appRepo.findOneByProfileAndAppVersionId(PROFILE_1, 1L)).thenReturn(APP_1);
        when(appRepo.findOneByProfileAndAppVersionId(PROFILE_1, 3L)).thenReturn(APP_2);
        when(appRepo.findOneByProfileAndAppVersionId(PROFILE_2, 1L)).thenReturn(APP_3);
        when(appRepo.findOneByProfileAndAppVersionId(PROFILE_2, 2L)).thenReturn(APP_4);

        when(accountClient.getAccount(65L)).thenReturn(ACCOUNT_1);
        when(accountClient.getAccount(66L)).thenReturn(ACCOUNT_2);

        when(vehicleClient.getVehicle("VIN111")).thenReturn(VEHICLE_1);
        when(vehicleClient.getVehicle("VIN222")).thenReturn(VEHICLE_2);
    }

    @Test
    public void getProfile() {
        Profile profile = profileUtility.getProfile(1L);
        assertNotNull(profile);
        assertThat(profile.getVin(), Matchers.equalTo("VIN111"));


        profile = profileUtility.getProfile(2L);
        assertNotNull(profile);
        assertThat(profile.getVin(), Matchers.equalTo("VIN222"));

        try {
            profileUtility.getProfile(null);
        }catch (ProfileException ex){
            assertEquals(ProfileExceptionCode.INVALID_INPUT.getCode(), ex.getReasonCode());
        }

        try {
            profileUtility.getProfile(3L);
        }catch (ProfileException ex){
            assertEquals(ProfileExceptionCode.PROFILE_NOT_FOUND.getCode(), ex.getReasonCode());
        }

    }

    @Test
    public void getAppVersion() {
        App app = profileUtility.getAppVersion(1L, PROFILE_1);
        assertNotNull(app);
        assertThat(app.getId(), Matchers.equalTo(1L));
        assertThat(app.getName(), Matchers.equalTo("Starbucks"));

        app = profileUtility.getAppVersion(3L, PROFILE_1);
        assertNotNull(app);
        assertThat(app.getId(), Matchers.equalTo(2L));
        assertThat(app.getName(), Matchers.equalTo("Glympse"));

        app = profileUtility.getAppVersion(1L, PROFILE_2);
        assertNotNull(app);
        assertThat(app.getId(), Matchers.equalTo(3L));
        assertThat(app.getName(), Matchers.equalTo("Starbucks"));

        app = profileUtility.getAppVersion(2L, PROFILE_2);
        assertNotNull(app);
        assertThat(app.getId(), Matchers.equalTo(4L));
        assertThat(app.getName(), Matchers.equalTo("Tim Hortons"));

        try {
            profileUtility.getAppVersion(1L, null);
        }catch (ProfileException ex){
            assertEquals(ProfileExceptionCode.INVALID_INPUT.getCode(), ex.getReasonCode());
        }
        try {
            profileUtility.getAppVersion(null, PROFILE_1);
        }catch (ProfileException ex){
            assertEquals(ProfileExceptionCode.INVALID_INPUT.getCode(), ex.getReasonCode());
        }
        try {
            profileUtility.getAppVersion(null, null);
        }catch (ProfileException ex){
            assertEquals(ProfileExceptionCode.INVALID_INPUT.getCode(), ex.getReasonCode());
        }

        try {
            profileUtility.getAppVersion(40L, PROFILE_1);
        }catch (ProfileException ex){
            assertEquals(ProfileExceptionCode.APP_NOT_FOUND.getCode(), ex.getReasonCode());
        }
    }

    @Test
    public void getAccount() {
        AccountDetails account = profileUtility.getAccount(65L);
        assertNotNull(account);
        assertThat(account.getUserDetails().getFirstName(), Matchers.equalTo("Zac"));
        assertThat(account.getUserDetails().getLastName(), Matchers.equalTo("Grantham"));
        assertThat(account.getUserDetails().getEmail(), Matchers.equalTo("zac.grantham@gm.com"));

        account = profileUtility.getAccount(66L);
        assertNotNull(account);
        assertThat(account.getUserDetails().getFirstName(), Matchers.equalTo("Aaron"));
        assertThat(account.getUserDetails().getLastName(), Matchers.equalTo("Johnson"));
        assertThat(account.getUserDetails().getEmail(), Matchers.equalTo("aaron.johnson@gm.com"));

        try {
            profileUtility.getAccount(null);
        }catch (ProfileException ex){
            assertEquals(ProfileExceptionCode.INVALID_INPUT.getCode(), ex.getReasonCode());
        }

        try {
            profileUtility.getAccount(40L);
        }catch (ProfileException ex){
            assertEquals(ProfileExceptionCode.ACCOUNT_NOT_FOUND.getCode(), ex.getReasonCode());
        }
    }

    @Test
    public void getVehicle() {
        Vehicle vehicle = profileUtility.getVehicle("VIN111");
        assertNotNull(vehicle);
        assertThat(vehicle.getYear(), Matchers.equalTo(2018));
        assertThat(vehicle.getMake(), Matchers.equalTo("Chevrolet"));
        assertThat(vehicle.getModel(), Matchers.equalTo("Malibu"));

        vehicle = profileUtility.getVehicle("VIN222");
        assertNotNull(vehicle);
        assertThat(vehicle.getYear(), Matchers.equalTo(2018));
        assertThat(vehicle.getMake(), Matchers.equalTo("Cadillac"));
        assertThat(vehicle.getModel(), Matchers.equalTo("ATS"));

        try {
            profileUtility.getVehicle(null);
        }catch (ProfileException ex){
            assertEquals(ProfileExceptionCode.INVALID_INPUT.getCode(), ex.getReasonCode());
        }

        try {
            profileUtility.getVehicle("NOT-A-VIN");
        }catch (ProfileException ex){
            assertEquals(ProfileExceptionCode.VEHICLE_NOT_FOUND.getCode(), ex.getReasonCode());
        }
    }
}