package com.gm.gsmc.pix.profiles.manager;

import com.gm.gsmc.pix.model.profile.ProfileDetails;
import com.gm.gsmc.pix.model.profile.ProfileStatus;
import com.gm.gsmc.pix.profiles.exception.ProfileException;
import com.gm.gsmc.pix.profiles.exception.ProfileExceptionCode;
import com.gm.gsmc.pix.profiles.jpa.ProfileRepository;
import com.gm.gsmc.pix.profiles.jpa.model.Profile;
import com.gm.gsmc.pix.profiles.translator.ProfileTranslator;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;

import java.util.List;

import static com.gm.gsmc.pix.profiles.TestData.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class ProfilesManagerTest {

    @InjectMocks
    private ProfilesManager manager;
    @Mock
    private ProfileTranslator profilesTranslator;
    @Mock
    private ProfileUtility profileUtility;
    @Mock
    private ProfileRepository profileRepo;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);

        when(profileRepo.findAllByAccountId(65L)).thenReturn(ACCOUNT_1_PROFILES);
        when(profileRepo.findAllByAccountId(66L)).thenReturn(ACCOUNT_2_PROFILES);
        when(profilesTranslator.translate(ACCOUNT_1_PROFILES)).thenReturn(ACCOUNT_1_PROFILE_DETAILS);
        when(profilesTranslator.translate(ACCOUNT_2_PROFILES)).thenReturn(ACCOUNT_2_PROFILE_DETAILS);

        when(profileRepo.findAllByVinAndStatus("VIN111", ProfileStatus.Active)).thenReturn(VEHICLE_1_PROFILES);
        when(profileRepo.findAllByVinAndStatus("VIN222", ProfileStatus.Active)).thenReturn(VEHICLE_2_PROFILES);
        when(profilesTranslator.translate(VEHICLE_1_PROFILES)).thenReturn(VEHICLE_1_PROFILE_DETAILS);
        when(profilesTranslator.translate(VEHICLE_2_PROFILES)).thenReturn(VEHICLE_2_PROFILE_DETAILS);

        when(profileRepo.findOneByAccountIdAndVinIgnoreCase(65L, "VIN111")).thenReturn(PROFILE_1);
        when(profileRepo.findOneByAccountIdAndVinIgnoreCase(66L, "VIN222")).thenReturn(PROFILE_2);
        when(profileRepo.findOneByAccountIdAndVinIgnoreCase(66L, "VIN111")).thenReturn(PROFILE_3);

        when(profileUtility.getProfile(1L)).thenReturn(PROFILE_1);
        when(profileUtility.getProfile(2L)).thenReturn(PROFILE_2);
        when(profileUtility.getProfile(3L)).thenReturn(PROFILE_3);
        when(profilesTranslator.translate(PROFILE_1)).thenReturn(PROFILE_DETAILS_1);
        when(profilesTranslator.translate(PROFILE_2)).thenReturn(PROFILE_DETAILS_2);
        when(profilesTranslator.translate(PROFILE_3)).thenReturn(PROFILE_DETAILS_3);

        when(profileRepo.save(any(Profile.class))).thenAnswer((Answer<Profile>) invocation -> {
            Object[] args = invocation.getArguments();
            return (Profile) args[0];
        });
    }

    @Test
    public void getAccountProfiles() {
        List<ProfileDetails> profiles = manager.getAccountProfiles(65L);
        assertNotNull(profiles);
        assertThat(profiles.size(), Matchers.equalTo(1));

        profiles = manager.getAccountProfiles(66L);
        assertNotNull(profiles);
        assertThat(profiles.size(), Matchers.equalTo(2));

        profiles = manager.getAccountProfiles(80L);
        assertNotNull(profiles);
        assertThat(profiles.size(), Matchers.equalTo(0));

        try {
            manager.getAccountProfiles(null);
        }catch (ProfileException ex){
            assertEquals(ProfileExceptionCode.INVALID_INPUT.getCode(), ex.getReasonCode());
        }
    }


    @Test
    public void getVehicleActiveProfiles() {
        List<ProfileDetails> profiles = manager.getVehicleActiveProfiles("VIN111");
        assertNotNull(profiles);
        assertThat(profiles.size(), Matchers.equalTo(2));

        profiles = manager.getVehicleActiveProfiles("VIN222");
        assertNotNull(profiles);
        assertThat(profiles.size(), Matchers.equalTo(1));

        profiles = manager.getVehicleActiveProfiles("VIN1234");
        assertNotNull(profiles);
        assertThat(profiles.size(), Matchers.equalTo(0));

        try {
            manager.getVehicleActiveProfiles(null);
        }catch (ProfileException ex){
            assertEquals(ProfileExceptionCode.INVALID_INPUT.getCode(), ex.getReasonCode());
        }
    }

    @Test
    public void getProfileId() {
        Long profileId = manager.getProfileId(65L, "VIN111");
        assertThat(profileId, Matchers.equalTo(1L));

        profileId = manager.getProfileId(66L, "VIN222");
        assertThat(profileId, Matchers.equalTo(2L));

        profileId = manager.getProfileId(66L, "VIN111");
        assertThat(profileId, Matchers.equalTo(3L));

        try {
            manager.getProfileId(null, null);
        }catch (ProfileException ex){
            assertEquals(ProfileExceptionCode.INVALID_INPUT.getCode(), ex.getReasonCode());
        }
        try {
            manager.getProfileId(65L, null);
        }catch (ProfileException ex){
            assertEquals(ProfileExceptionCode.INVALID_INPUT.getCode(), ex.getReasonCode());
        }
        try {
            manager.getProfileId(null, "VIN111");
        }catch (ProfileException ex){
            assertEquals(ProfileExceptionCode.INVALID_INPUT.getCode(), ex.getReasonCode());
        }
        try {
            manager.getProfileId(111L, "NOT-A-VIN");
        }catch (ProfileException ex){
            assertEquals(ProfileExceptionCode.PROFILE_NOT_FOUND.getCode(), ex.getReasonCode());
        }
    }

    @Test
    public void getProfileDetails() {
        ProfileDetails profile = manager.getProfileDetails(1L);
        assertNotNull(profile);
        assertThat(profile.getAccountId(), Matchers.equalTo(65L));
        assertThat(profile.getVin(), Matchers.equalTo("VIN111"));

        profile = manager.getProfileDetails(2L);
        assertNotNull(profile);
        assertThat(profile.getAccountId(), Matchers.equalTo(66L));
        assertThat(profile.getVin(), Matchers.equalTo("VIN222"));

        profile = manager.getProfileDetails(3L);
        assertNotNull(profile);
        assertThat(profile.getAccountId(), Matchers.equalTo(66L));
        assertThat(profile.getVin(), Matchers.equalTo("VIN111"));

        try {
            manager.getProfileDetails(4L);
        } catch (ProfileException ex) {
            assertEquals(ProfileExceptionCode.PROFILE_NOT_FOUND.getCode(), ex.getReasonCode());
        }
        try {
            manager.getProfileDetails(null);
        }catch (ProfileException ex){
            assertEquals(ProfileExceptionCode.INVALID_INPUT.getCode(), ex.getReasonCode());
        }
    }

    @Test
    public void getProfileStatus() {
        ProfileStatus status = manager.getProfileStatus(1L);
        assertNotNull(status);
        assertThat(status.toString(), Matchers.equalTo("Active"));

        status = manager.getProfileStatus(2L);
        assertNotNull(status);
        assertThat(status.toString(), Matchers.equalTo("Active"));

        status = manager.getProfileStatus(3L);
        assertNotNull(status);
        assertThat(status.toString(), Matchers.equalTo("Active"));
    }

    @Test
    public void activateProfile() {
        ProfileDetails profile = manager.activateProfile(1L);
        assertNotNull(profile);
    }

    @Test
    public void deactivateProfile() {
        ProfileDetails profile = manager.deactivateProfile(1L);
        assertNotNull(profile);
    }

    @Test
    public void advisorRemoveProfile() {
        ProfileDetails profile = manager.advisorRemoveProfile(1L);
        assertNotNull(profile);
    }

    @Test
    public void closeAccountProfiles() {
        List<ProfileDetails> profiles = manager.closeAccountProfiles(65L);
        assertNotNull(profiles);

        profiles = manager.closeAccountProfiles(66L);
        assertNotNull(profiles);

        profiles = manager.closeAccountProfiles(67L);
        assertNotNull(profiles);
        assertEquals(0, profiles.size());

        try {
            manager.closeAccountProfiles(null);
        } catch (ProfileException ex) {
            assertEquals(ProfileExceptionCode.INVALID_INPUT.getCode(), ex.getReasonCode());
        }
    }

    @Test
    public void closeVehicleProfiles() {
        List<ProfileDetails> profiles = manager.closeVehicleProfiles("VIN111");
        assertNotNull(profiles);

        profiles = manager.closeVehicleProfiles("VIN222");
        assertNotNull(profiles);

        profiles = manager.closeVehicleProfiles("VIN1234");
        assertNotNull(profiles);
        assertEquals(0, profiles.size());

        try {
            manager.closeVehicleProfiles(null);
        } catch (ProfileException ex) {
            assertEquals(ProfileExceptionCode.INVALID_INPUT.getCode(), ex.getReasonCode());
        }
    }

    @Test
    public void createProfile() {
        assert true;
    }
}