package com.gm.gsmc.pix.profiles.manager;

import com.gm.gsmc.pix.model.appcatalog.Application;
import com.gm.gsmc.pix.profiles.client.server.AccountClient;
import com.gm.gsmc.pix.profiles.client.server.AppAvailabilityClient;
import com.gm.gsmc.pix.profiles.client.server.AppCatalogClient;
import com.gm.gsmc.pix.profiles.jpa.AppRepository;
import com.gm.gsmc.pix.profiles.jpa.ProfileRepository;
import com.gm.gsmc.pix.profiles.jpa.model.Profile;
import com.gm.gsmc.pix.profiles.translator.ProfileTranslator;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;

import java.util.List;

import static com.gm.gsmc.pix.profiles.TestData.*;
import static com.gm.gsmc.pix.profiles.TestData.PROFILE_DETAILS_3;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("Duplicates")
public class ProfileAppsManagerTest {

    @InjectMocks
    private ProfileAppsManager manager;
    @Mock
    private AppRepository appsRepo;
    @Mock
    private ProfileRepository profileRepo;
    @Mock
    private ProfileTranslator profilesTranslator;
    @Mock
    private AppCatalogClient appCatalogClient;
    @Mock
    private ProfileUtility profileUtility;


    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);

        when(profileUtility.getProfile(1L)).thenReturn(PROFILE_1);
        when(profileUtility.getProfile(2L)).thenReturn(PROFILE_2);
        when(profileUtility.getProfile(3L)).thenReturn(PROFILE_3);
        when(profilesTranslator.translate(PROFILE_1)).thenReturn(PROFILE_DETAILS_1);
        when(profilesTranslator.translate(PROFILE_2)).thenReturn(PROFILE_DETAILS_2);
        when(profilesTranslator.translate(PROFILE_3)).thenReturn(PROFILE_DETAILS_3);


        when(profileRepo.save(any(Profile.class))).thenAnswer((Answer<Profile>) invocation -> {
            Object[] args = invocation.getArguments();
            return (Profile) args[0];
        });
    }

    @Test
    public void getProfileAppsInstalled() {
        List<Application> apps = manager.getProfileAppsInstalled(1L);
        assertNotNull(apps);
        assertEquals(2, apps.size());

        apps = manager.getProfileAppsInstalled(2L);
        assertNotNull(apps);
        assertEquals(2, apps.size());

        apps = manager.getProfileAppsInstalled(3L);
        assertNotNull(apps);
        assertEquals(0, apps.size());
    }

}